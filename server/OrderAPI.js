const mysql = require("mysql");
const dotenv = require('dotenv');

dotenv.config();

const connectionorder = mysql.createConnection({
  host: process.env.DB_HOST,
  user: process.env.DB_USER,
  password: process.env.DB_PASS,
  database: process.env.DB_DATABASE,
});
connectionorder.connect();

const getFieldList = async (shop) => {
  try {
    return new Promise((resolve, reject) => {
      connectionorder.query(
        "SELECT id,fieldsname,active FROM fields where shop_url='" + shop + "'",
        async function (error, results, fields) {
          if (error) throw error;

          resolve(results);
        }
      );
    });
  } catch (err) {
    throw new Error(err);
  }
};
const getFieldListIsActive = async (shop) => {
  try {
    return new Promise((resolve, reject) => {
      connectionorder.query(
        "SELECT id,fieldsname,field_type,field_type FROM fields where shop_url='" +
          shop +
          "'and active=1",
        async function (error, results, fields) {
          if (error) throw error;

          resolve(results);
        }
      );
    });
  } catch (err) {
    throw new Error(err);
  }
};

const saveFieldData = async (key, value, shop) => {
  try {
    return new Promise((resolve, reject) => {
      connectionorder.query(
        "insert into frontdata (field,value,shop) VALUES ('" +
          key +
          "','" +
          value +
          "','" +
          shop +
          "');",
        async function (error, results, fields) {
          if (error) throw error;

          resolve(results);
        }
      );
    });
  } catch (err) {
    throw new Error(err);
  }
};




const isFieldAvail = async (fieldname, shop) => {
  try {
    return new Promise((resolve, reject) => {
      
      connectionorder.query("select id from fields where fieldsname='"+fieldname+"' and shop_url='"+shop+"'",
        async function (error, results, fields) {
          if (error) throw error;

          resolve(results);
        }
      );
    });
  } catch (err) {
    throw new Error(err);
  }
};

const saveFieldAdminData = async (fieldname, shop, active, field_type) => {
  try {
    return new Promise((resolve, reject) => {
      connectionorder.query(
        "insert into fields (fieldsname,shop_url,active,field_type) VALUES ('" +
          fieldname +
          "','" +
          shop +
          "','" +
          active +
          "','" +
          field_type +
          "');",
        async function (error, results, fields) {
          if (error) throw error;

          resolve(results);
        }
      );
    });
  } catch (err) {
    throw new Error(err);
  }
};

const deleteFieldAdminData = async (id, shop) => {
  try {
    return new Promise((resolve, reject) => {
      connectionorder.query(
        "delete from fields where id=" + id + " and shop_url='" + shop + "'",
        async function (error, results, fields) {
          if (error) throw error;

          resolve(results);
        }
      );
    });
  } catch (err) {
    throw new Error(err);
  }
};
const updateActiveField = async (active, id, shop) => {
  try {
    return new Promise((resolve, reject) => {
      connectionorder.query(
        "update fields set active=" +
          active +
          " where id=" +
          id +
          " and shop_url='" +
          shop +
          "'",
        async function (error, results, fields) {
          if (error) throw error;

          resolve(results);
        }
      );
    });
  } catch (err) {
    throw new Error(err);
  }
};
const getFieldsToListing = async (shop) => {
  try {
    return new Promise((resolve, reject) => {
      connectionorder.query(
        "SELECT id,fieldsname,field_type,field_type FROM fields where shop_url='" +
          shop +
          "'and active=1",
        async function (error, results, fields) {
          if (error) throw error;
          resolve(results);
        }
      );
    });
  } catch (err) {
    throw new Error(err);
  }
};

module.exports = {
  getFieldList,
  getFieldListIsActive,
  saveFieldData,
  saveFieldAdminData,
  deleteFieldAdminData,
  updateActiveField,
  getFieldsToListing,
  isFieldAvail,
};
