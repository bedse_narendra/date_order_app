import React, { useEffect, useState } from "react";
//import axios from "axios"
import Routes from "./Routes";

import { createMemoryHistory } from "history";
import { Router, Switch, Route, Link } from "react-router-dom";
import Index from "./index";
import img from "./img/arcafy.png";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
 import { faFile, faList, faBlog,faTag,faFileImport,faTrash,faAmericanSignLanguageInterpreting,faParagraph,faCogs, faCopy,faReceipt, faUser, faShoppingBag,faListAlt } from '@fortawesome/free-solid-svg-icons'
 import jwt from "jwt-decode"; // import dependency

import { getSessionToken } from "@shopify/app-bridge-utils";
import { useAppBridge } from "@shopify/app-bridge-react";
import App from "next/app";
import {CopyToClipboard} from 'react-copy-to-clipboard';
import { Alert} from 'react-bootstrap';

const history = createMemoryHistory();

const Setup = (props) => {
  const app = useAppBridge();
  let string = "";

  const [fieldlist, setFieldList] = useState([]);
  const [loader, setIsLoaded] = useState([]);
  const [error, setError] = useState([]);
  const [shop, setShop] = useState();
  const [fieldname, setFieldName] = useState('');
  //const [state, setState] = useState({value: 'string'});
  const [fieldVal, setFieldVal] = useState("text");
  const [active, setActive] = useState(0);
  const [success, setSuccess] = useState(null);
  const [successError, setSuccessError] = useState(null);
  const [fieldNotNull, setFieldNotNull] = useState(null);
  const [liquidfileNotAvail, setLiquidFileNotFound] = useState(null);
  const [snippetCode, setSnippetCode] = useState(false);
  const [statecopied, setStateCopied] = useState({
    value: '',
    copied: false,
  });

  
  
  const [successdelete, setSuccessDelete] = useState(null);
  const [successupdate, setSuccessUpdate] = useState(null);
  const [host, setHost] = useState();
  const [isLoading, setIsLoading] = useState(false);
  const [scriptnote, setScript] = useState();

  //console.log(scriptnote)
  
  useEffect(() => {
    sessionDecode();

    getFieldList();
    getFieldListActive();
  }, []);
  const sessionDecode = async () => {
    const token = await getSessionToken(app);
    const user_token = jwt(token);

    const hostdecode = user_token.iss.replace(/https:\/\//, "");
    setShop(hostdecode.replace("/admin", ""));

    const encodedString = Buffer.from(hostdecode).toString("base64");
    setHost(encodedString.replace("==", ""));
  };
  const getFieldListActive = async () => {
    setIsLoading(true);
    setStateCopied({
      value: '',
      copied: false,
    });
  
    try {
      const token = await getSessionToken(app);

      const res = await fetch("/getFieldsIsActive", {
        headers: { Authorization: `Bearer ${token}` },
      });
      const responseData = await res.json();

      if (responseData.status === "EMPTY_SETTINGS") {
        return;
      }

      if (responseData.status === "OK_SETTINGS") {
        responseData.data.map((todo) => {

          if(todo.field_type == 'text')
          {
          let scriptDynamic = `
          <p class="cart-attribute__field">
          <label for="${todo.fieldsname}">${todo.fieldsname}</label>
          <input id="${todo.fieldsname.split(" ").join("_")}" type="${
            todo.field_type
          }" maxlength="100" name="attributes[${todo.fieldsname
            .split(" ")
            .join("_")}]" value="{{ cart.attributes["${todo.fieldsname
            .split(" ")
            .join("_")}"] }}">
        </p>`;

          string += scriptDynamic;
            }
            else{
              let scriptDynamic = `
              <p class="cart-attribute__field">
              <label for="${todo.fieldsname}">${todo.fieldsname}</label>
              <input id="${todo.fieldsname.split(" ").join("_")}" type="${
                todo.field_type
              }" name="attributes[${todo.fieldsname
                .split(" ")
                .join("_")}]" value="{{ cart.attributes["${todo.fieldsname
                .split(" ")
                .join("_")}"] }}">
            </p>`;
    
              string += scriptDynamic;

            }


        });
        //  setScript(string);
       // console.log(string)
        setScript("<div class='cart-fields' style='float:right;padding-top: 10px;'>"+string+"</div>")
        shopifyScriptLoad(string);
        return;
      }

      throw Error("Unknown settings status");
    } catch (err) {
      setError(err.message);
    } finally {
      setIsLoading(false);
    }
  };
  const shopifyScriptLoad = async (scriptlaod) => {
    // console.log(scriptlaod)
    setIsLoading(true);
    try {
      const token = await getSessionToken(app);
      const res = await fetch("/shopifyScriptCartPageCode", {
        headers: { Authorization: `Bearer ${token}` },
      });

      const responseData = await res.json();

     // console.log(responseData.status)


      if (responseData.status === "500") {

        setLiquidFileNotFound(responseData.data)

        setSuccessDelete(null);
        setSuccessUpdate(null);
        setSuccess(null);
        setFieldNotNull(null)
        setSuccessError(null);
        setSnippetCode(true)
        return;
      }

      if (responseData.status === "EMPTY_SETTINGS") {
        return;
      }

      if (responseData.status === "OK_SETTINGS") {
        setLiquidFileNotFound(null)
        setSnippetCode(false)
         console.log(responseData.data.asset.value)
        var index = responseData.data.asset.value.indexOf(
          "</form>"
        ); // Gets the first index where a space occours

        var firstpart = responseData.data.asset.value.substr(0, index); // Gets the first part

    // console.log("firstpart", firstpart);

         var index_sub = firstpart.indexOf("<div class='cart-fields'"); // Gets the first index where a space occours
         var firstpart_sub = firstpart.substr(0, index_sub); // Gets the first part
       //  console.log("firstpart_sub", firstpart_sub);

        var secondpart = responseData.data.asset.value.substr(index + 1); // Gets the text part

        // console.log('firstpart_sub'+firstpart_sub)
       // console.log("secondpart" + secondpart);
      if(firstpart_sub == '')
      {
        const concatScript =
          firstpart+"<div class='cart-fields' style='float:right;padding-top: 10px;'>"+scriptlaod+"</div><"+secondpart ;
         
          scriptLoading(concatScript, responseData.data.asset.theme_id);
         // console.log("concatScript--", concatScript);
      }else{
          const concatScript =
          firstpart_sub+"<div class='cart-fields' style='float:right;padding-top: 10px;'>"+scriptlaod+"</div><"+secondpart ;
         
          scriptLoading(concatScript, responseData.data.asset.theme_id);
        //  console.log("concatScript--", concatScript);
      }
        return;
      }

      throw Error("Unknown settings status");
    } catch (err) {
      setError(err.message);
    } finally {
      setIsLoading(false);
    }
  };
  const scriptLoading = async (script, theme_id) => {
    setIsLoading(true);
   
    try {
      const token = await getSessionToken(app);
      const res = await fetch("/scriploaded", {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          script: script,
          theme_id: theme_id,
        }),
      });
      const responseData = await res.json();
      if (responseData.status === "EMPTY_SETTINGS") {
        return;
      }

      if (responseData.status === "OK_SETTINGS") {
        console.log(responseData.data);
        return;
      }

      throw Error("Unknown settings status");
    } catch (err) {
      setError(err.message);
    } finally {
      setIsLoading(false);
    }
  };

  const getFieldList = async () => {
    setIsLoading(true);

    try {
      const token = await getSessionToken(app);
      const res = await fetch("/getFields", {
        headers: { Authorization: `Bearer ${token}` },
      });
      const responseData = await res.json();
      if (responseData.status === "EMPTY_SETTINGS") {
        return;
      }

      if (responseData.status === "OK_SETTINGS") {
        setFieldList(responseData.data);
        return;
      }

      throw Error("Unknown settings status");
    } catch (err) {
      setError(err.message);
    } finally {
      setIsLoading(false);
    }
  };
  const handleChange = async (e) => {
    if (e.target.getAttribute("type") == "checkbox") {
      if (e.target.checked == true) {
        setActive(1);
      } else {
        setActive(0);
      }
    }
  };
  const handleChangeActiveField = async (e, id) => {
    if (e.target.getAttribute("type") == "checkbox") {
      if (e.target.checked == true) {
        updateActiveFieldValue(1, id);
      } else {
        updateActiveFieldValue(0, id);
      }
    }
  };

  const updateActiveFieldValue = async (active, id) => {
    setIsLoading(true);
    try {
      const token = await getSessionToken(app);

      const res = await fetch("/updateActiveField", {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify({ active: active, id: id }),
      });

      const responseData = await res.json();

      if (responseData.status === "200") {
        setSuccessUpdate(responseData.success);
        setSuccess(null);
        setFieldList([]);
        getFieldList();
        getFieldListActive();
      }

      throw Error("Unknown settings status");
    } catch (err) {
      setError(err.message);
    } finally {
      setIsLoading(false);
    }
  };
  const deletefieldById = async (id) => {
    setIsLoading(true);
    try {
      const token = await getSessionToken(app);

      const res = await fetch("/deletefield", {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify({ id: id }),
      });

      const responseData = await res.json();

      if (responseData.status === "200") {
        setSuccessDelete(responseData.success);
        setSuccess(null);
        setFieldList([]);
        getFieldList();
        getFieldListActive();
      }

      throw Error("Unknown settings status");
    } catch (err) {
      setError(err.message);
    } finally {
      setIsLoading(false);
    }
  };

  const handleSubmit = async (e) => {
    setIsLoading(true);

    e.preventDefault();
    e.target.reset();

    if(fieldname == '')
    {
      setFieldNotNull('Please Enter Field Name')
      setSuccessDelete(null);
      setSuccessUpdate(null);
      setSuccess(null);
      setSuccessError(null);
      setLiquidFileNotFound(null)
      return 0;
    }

    try {
      const token = await getSessionToken(app);

      const res = await fetch("/savefieldsadmindata", {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          fieldsname: fieldname,
          field_type: fieldVal,
          active: active,
        }),
      });

      const responseData = await res.json();

      if (responseData.status === "200") {
        setSuccessDelete(null);
        setSuccessUpdate(null);
        setSuccess(responseData.success);
        setFieldNotNull(null)
        setSuccessError(null);
        setLiquidFileNotFound(null)

        setFieldList([]);
        getFieldList();
        getFieldListActive();
      }
      if (responseData.status === "201") {
        setSuccessDelete(null);
        setSuccessUpdate(null);
        setFieldNotNull(null)
        setSuccess(null);
        setLiquidFileNotFound(null)

        setSuccessError(responseData.success);
        setFieldList([]);
        getFieldList();
        getFieldListActive();
      }

      throw Error("Unknown settings status");
    } catch (err) {
      setError(err.message);
    } finally {
      setIsLoading(false);
    }
  };
  return (
    <div>
      <div style={{ backgroundColor: "white" }}>

      <div className="header">
            <div
              className="align-items-center justify-content-between text-center container"
              style={{ marginTop: "5px" }}
            >
              <div className="logo my-0 font-weight-normal h3 ">
                <img
                  style={{ float: "left", height: "40px", paddingTop: "6px" }}
                  src={img}
                ></img>
                <span
                  className="text-center"
                  style={{
                    fontWeight: "bolder",
                    marginRight: "12%",
                    paddingTop: "10px",
                    display: "inline-block",
                  }}
                >
                 Order Fields
                </span>
                <a
                  className="text-center btn"
                  target="_blank"
                  href={`/docs`}
                  style={{
                    float: "right",
                    fontWeight: "bold",
                    marginTop: 10,
                    fontSize: "16px",
                    display: "inline-block",
                    background: "#FFE941",
                    color: "#022866",
                  }}
                >
                  Support
                </a>
              </div>
              {/* <hr style={{marginTop:"1em",marginBottom:"0.5em"}}/> */}
            </div>
          </div>


      <Router history={history}>
            <div className="topnav">
              <a style={{paddingTop:"10px"}} href={"/?shop=" + shop + "&host=" + host}><FontAwesomeIcon icon={faTag}/><strong> Order List</strong></a>
              <a href={"/setup?shop=" + shop + "&host=" + host}>
              <button type="button" class="btn btn-primary" style={{height:"36px",paddingTop:"4px",width:"112%"}}>
              <FontAwesomeIcon icon={faCogs}/> Setup
              </button>
              </a>
              <a  style={{
                 float:'right',
                 paddingTop:"10px"
                }}  href={"/plan?shop=" + shop + "&host=" + host}><FontAwesomeIcon icon={faList}/><strong> Plans</strong></a>
            </div>
          </Router>



      <form onSubmit={handleSubmit} style={{marginLeft:"5px"}}>
        <div
          className="form-group form-inline text-center"
          style={{ display: "inline-flex", paddingTop: "12px" }}
        >
          <label style={{ width: "80%", paddingTop: "12px" }}>
            {" "}
            Field Name{" "}
          </label>
          <input
            id="addTodo"
            name="fieldname"
            autoComplete="off"
            //defaultValue={fieldname}
            className="form-control "
            style={{ margin: "7px" }}
            placeholder="field name"
            onChange={(e) => setFieldName(e.target.value)}
            required
          />

          <label style={{ width: "77%", paddingTop: "12px" }}>
            {" "}
            Value Type{" "}
          </label>

          <select
            style={{ marginLeft: "0px" }}
            value={fieldVal}
            onChange={(e) => setFieldVal(e.target.value)}
          >
            <option value="text">Text</option>
            <option value="Integer">Integer</option>
            <option value="date">Date</option>
          </select>
          <label style={{ width: "60%", paddingTop: "12px" }}> Active </label>
          <input
            name="checkbox_static"
            id="checkbox_static"
            type="checkbox"
            defaultChecked={active}
            style={{ marginTop: "21px" }}
            onChange={handleChange}
          />

      
           <button  style={{ margin: "8px", backgroundColor: "#022866"}} className="btn btn-primary">Add</button>
        </div>
        </form>
        {success != null ? (
          <div
            class="alert alert-primary"
            role="alert"
            style={{ padding: "12px", margin: "0px", textAlign: "center" }}
          >
            {success}
          </div>
        ) : (
          ""
        )}






{successError != null ? (
          <div
            class="alert alert-warning"
            role="alert"
            style={{ padding: "12px", margin: "0px", textAlign: "center" }}
          >
            {successError}
          </div>
        ) : (
          ""
        )}

{fieldNotNull != null ? (
          <div
            class="alert alert-warning"
            role="alert"
            style={{ padding: "12px", margin: "0px", textAlign: "center" }}
          >
            {fieldNotNull}
          </div>
        ) : (
          ""
        )}



        {successupdate != null ? (
          <div
            class="alert alert-primary"
            role="alert"
            style={{ padding: "12px", margin: "0px", textAlign: "center" }}
          >
            {successupdate}
          </div>
        ) : (
          ""
        )}

        {successdelete != null ? (
          <div
            class="alert alert-warning"
            role="alert"
            style={{ padding: "12px", margin: "0px", textAlign: "center" }}
          >
            {successdelete}
          </div>
        ) : (
          ""
        )}

        <h4 style={{ textAlign: "center", paddingTop: "10px" }}>Fields</h4>
        {/* <div className="container py-15">
          <div className="add-todos mb-5"> */}
            <div style={{ margin: "16px" }} className="table-responsive">
              <table className="table table-bordered table-striped table-highlight">
                <tbody>
                  <tr style={{ background: "#000000" }}>
                    <th
                      className="text-center"
                      style={{ width: "20%", color: "#ffffff" }}
                    >
                      Order
                    </th>
                    <th
                      className="text-center"
                      style={{ width: "20%", color: "#ffffff" }}
                    >
                      Field Name
                    </th>

                    <th
                      className="text-center"
                      style={{ width: "20%", color: "#ffffff" }}
                    >
                      Enable
                    </th>
                    <th
                      className="text-center"
                      style={{ width: "20%", color: "#ffffff" }}
                    >
                      Delete
                    </th>
                  </tr>

                  {fieldlist.map((todo, index) => (
                    <>
                      <tr>
                        <td className="text-center"> {todo.id}</td>
                        <td className="text-center"> {todo.fieldsname}</td>
                        <td className="text-center">
                          <input
                            name="checkbox"
                            id="products"
                            type="checkbox"
                            defaultChecked={todo.active}
                            onChange={(e) =>
                              handleChangeActiveField(e, todo.id)
                            }
                          />
                        </td>
                        <td className="text-center">
                          <button
                            style={{ backgroundColor: "#022866" }}
                            className="btn btn-primary"
                            onClick={() => deletefieldById(todo.id)}
                          >
                            Delete
                          </button>
                        </td>
                      </tr>
                    </>
                  ))}
                </tbody>
              </table>
            </div>
           


            {liquidfileNotAvail != null ? (
          <div
            class="alert alert-warning"
            role="alert"
            style={{ padding: "12px", margin: "0px", textAlign: "center" }}
          >
            {liquidfileNotAvail}
          </div>
        ) : (
          ""
        )}

{snippetCode ?

<Alert  variant="secondary">{scriptnote}
 
         <CopyToClipboard text={scriptnote}
         onCopy={() => setStateCopied({copied: true})}>
           <span> {statecopied.copied == true ? <span  style={{color: 'rgb(12 218 9 / 1)',float:'right'}}>Copied</span> : <a> <FontAwesomeIcon style={{float:'right'}} icon={faCopy} /></a>}</span>
         </CopyToClipboard>
         
        </Alert> 
         :""}

         {/* <CopyToClipboard text={statecopied.value}
          onCopy={() => setStateCopied({copied: true})}>
          <span>Copy to clipboard with span</span>
        </CopyToClipboard> */}

            {/* {snippetCode ? (
         {scriptnote}
        ) : (
          ""
        )} */}

            <div className="header">
              <div
                className="align-items-center justify-content-between text-center container"
                style={{ marginTop: "5px" }}
              >
                <div className="logo my-0 font-weight-normal ">
                  <span style={{ marginLeft: "20px" }} className="text-center">
                  Order Field - Powered by Arcafy
                  </span>
                </div>
                {/* <hr style={{marginTop:"1em",marginBottom:"0.5em"}}/> */}
              </div>
            </div>
          </div>
        </div>
    //   </div>
    // </div>
  );
};
export default Setup;
