import React, { Component } from "react";

import Orderlist from "./orderlist";
import { Router, Route, Link, Switch } from "react-router-dom";
 import Plan from "./plan";
 import Setup from "./setup";
import { createMemoryHistory } from "history";
const history = createMemoryHistory();

export default class Routes extends Component {
  render() {
    return (
      <>
        <Router history={history}>
          <Switch>

            <Route path="/" exact component={Orderlist} />
            {/* <Route path="/" component={Plan} /> */}
            {/* <Route path="/" exact component={Setup} /> */}

            <Route path="/setup" component={Setup} />
            <Route path="/plan" component={Plan} /> 
          </Switch>
        </Router>
      </>
    );
  }
}
