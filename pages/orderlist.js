import React, { useEffect, useState } from "react";
//import axios from "axios"
import Routes from "./Routes";
import img from "./img/arcafy.png";
import $ from "jquery";

import { createMemoryHistory } from "history";
import { Router, Switch, Route, Link } from "react-router-dom";
const history = createMemoryHistory();
import { getSessionToken } from "@shopify/app-bridge-utils";
import { useAppBridge } from "@shopify/app-bridge-react";
import jwt from "jwt-decode"; // import dependency
const Orderlist = () => {
  const app = useAppBridge();

  const [shopOrderData, setOrderData] = useState([]);
  const [error, setError] = useState([]);
  const [shop, setShop] = useState();
  const [host, setHost] = useState();
  const [isLoading, setIsLoading] = useState(false);
  const [fieldToListing, setOrderFieldListingData] = useState([]);
  const [state, setState] = useState({ value: "single_line_text_field" });
  const [fieldVal, setFieldVal] = useState("string");
  const [fieldname, setFieldName] = useState("");
  const [showPerPage, setShowPerPage] = useState(5);

  const [pagination, setPagination] = useState({
    start: 0,
    end: showPerPage,
  });

  const [paginationp, setPaginationp] = useState({
    start: 0,
    end: showPerPage,
  });

  const onPaginationChange = (start, end) => {
    setPagination({ start: start, end: end });
  };

  const onPaginationPChange = (start, end) => {
    setPaginationp({ start: start, end: end });
  };

  const compareByAsc = (key) => {
    return function (a, b) {
      if (a[key] < b[key]) return -1;
      if (a[key] > b[key]) return 1;
      return 0;
    };
  };

  const compareByDesc = (key) => {
    return function (a, b) {
      if (a[key] < b[key]) return 1;
      if (a[key] > b[key]) return -1;
      return 0;
    };
  };

  const sortBy = (key) => {
    let arrayCopy = [...shopOrderData];

    const arrInStr = JSON.stringify(arrayCopy);
    arrayCopy.sort(compareByAsc(key));
    const arrInStr1 = JSON.stringify(arrayCopy);
    if (arrInStr === arrInStr1) {
      arrayCopy.sort(compareByDesc(key));
    }
    //console.log(arrayCopy);
    setOrderData(arrayCopy);
  };

  useEffect(() => {
    sessionDecode();
    getOrderList();
    getFieldsToListing();
  }, []);

  const sessionDecode = async () => {
    const token = await getSessionToken(app);
    const user_token = jwt(token);

    const hostdecode = user_token.iss.replace(/https:\/\//, "");
    setShop(hostdecode.replace("/admin", ""));

    const encodedString = Buffer.from(hostdecode).toString("base64");
    setHost(encodedString.replace("==", ""));
  };
  const getOrderList = async () => {
    setIsLoading(true);

    try {
      const token = await getSessionToken(app);
      const res = await fetch("/getOrder", {
        headers: { Authorization: `Bearer ${token}` },
      });
      const responseData = await res.json();

      if (responseData.status === "OK_SETTINGS") {
       // console.log(responseData.data);
        setOrderData(responseData.data);
        return;
      }

      if (responseData.status === "EMPTY_SETTINGS") {
        return;
      }

      throw Error("Unknown settings status");
    } catch (err) {
      setError(err.message);
    } finally {
      setIsLoading(false);
    }
  };

  const getFieldsToListing = async () => {
    setIsLoading(true);

    try {
      const token = await getSessionToken(app);
      const res = await fetch("/getFieldsToListing", {
        headers: { Authorization: `Bearer ${token}` },
      });
      const responseData = await res.json();

      if (responseData.status === "EMPTY_SETTINGS") {
        return;
      }

      if (responseData.status === "OK_SETTINGS") {
        //  setOrderDataByFieldName([])

        setOrderFieldListingData(responseData.data);

        return;
      }

      throw Error("Unknown settings status");
    } catch (err) {
      setError(err.message);
    } finally {
      setIsLoading(false);
    }
  };

  const handleChange = async (event) => {
    const order_id = event.target.value;
    setState({ value: order_id });
    const fieldnameval = $(".attr_" + order_id).val();
    setFieldName(fieldnameval);

   // console.log(order_id);
  };

  return (
    // <div className="container py-15">
    //   <div className="add-todos mb-5">
        <div >
         
         

          <h5
            style={{ width: "100%", marginTop: "10px" }}
            className="text-center"
          >
            {" "}
            <strong> Customer Order List</strong>
          </h5>

          <div style={{ margin: "0px" }} className="table-responsive">
            <table className="table table-bordered table-striped table-highlight">
              <tbody>
                <tr style={{ background: "#000000" }}>
                  <th
                    onClick={() => sortBy("order_number")}
                    className="text-center"
                    style={{ width: "15%", color: "#ffffff" }}
                  >
                    Order Number
                  </th>
                  <th
                    onClick={() => sortBy("email")}
                    className="text-center"
                    style={{ width: "20%", color: "#ffffff" }}
                  >
                    Name/ Email
                  </th>

                  <th
                    onClick={() => sortBy("created_at")}
                    className="text-center"
                    style={{ width: "20%", color: "#ffffff" }}
                  >
                    Order Date
                  </th>
                  {fieldToListing.map((fieldlist, index) => (
                    <th
                      onClick={() =>
                        sortBy(fieldlist.fieldsname.split(" ").join("_"))
                      }
                      className="text-center"
                      style={{ width: "15%", color: "#ffffff" }}
                    >
                      {fieldlist.fieldsname}
                    </th>
                  ))}
                </tr>

                {/* {shopOrderData.slice(paginationp.start, paginationp.end).map((todo, index) => ( */}
                {shopOrderData.map((todo, index) => (
                  <>
                    <tr>
                      <td className="text-center"> {todo.order_number}</td>

                      <td className="text-center">
                        {" "}
                        {todo.first_name} {todo.last_name}{" "}
                        {todo.first_name == "" ? "" : "/"} {todo.email}
                      </td>
                      <td className="text-center"> {todo.created_at}</td>

                      {fieldToListing.map((fieldlist, index) => (
                        <>
                          {(() => {
                            var field = fieldlist.fieldsname
                              .split(" ")
                              .join("_");

                            if (true) {
                              return (
                                <td className="text-center"> {todo[field]}</td>
                              );
                            }
                          })()}
                        </>
                      ))}
                    </tr>
                  </>
                ))}
              </tbody>
            </table>
          </div>

          {/* <PageProducts
         showPerPage={showPerPage}
         onPaginationChange={onPaginationPChange}
         total={shopOrderData.length}
       /> */}

          <div className="header">
            <div
              className="align-items-center justify-content-between text-center container"
              style={{ marginTop: "5px" }}
            >
              <div className="logo my-0 font-weight-normal ">
                <span style={{ marginLeft: "20px" }} className="text-center">
                Order Field - Powered by Arcafy

                </span>
              </div>
              {/* <hr style={{marginTop:"1em",marginBottom:"0.5em"}}/> */}
            </div>
          </div>
        {/* </div>
      </div> */}

    </div>
  );
};
export default Orderlist;
