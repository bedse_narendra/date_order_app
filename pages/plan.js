import React, { useEffect, useState } from "react";

import Routes from "./Routes";
import $ from "jquery";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
 import { faFile, faList, faBlog,faTag,faFileImport,faTrash,faAmericanSignLanguageInterpreting,faParagraph,faCogs, faReceipt, faUser, faShoppingBag,faListAlt } from '@fortawesome/free-solid-svg-icons'
 
import { createMemoryHistory } from "history";
import { Router, Switch, Route, Link } from "react-router-dom";
const history = createMemoryHistory();
import img from "./img/arcafy.png";
import { getSessionToken } from "@shopify/app-bridge-utils";
import { useAppBridge } from "@shopify/app-bridge-react";

import jwt from "jwt-decode"; // import dependency
const Plan =()=>{
    const app = useAppBridge();

  const [shop, setShop] = useState();
  const [host, setHost] = useState();

  useEffect(() => {
    sessionDecode();
  });

  const sessionDecode = async () => {
    const token = await getSessionToken(app);
    const user_token = jwt(token);

    const hostdecode = user_token.iss.replace(/https:\/\//, "");
    setShop(hostdecode.replace("/admin", ""));

    const encodedString = Buffer.from(hostdecode).toString("base64");
    setHost(encodedString.replace("==", ""));
  };
return(
    // <div className="container py-15">
    // <div className="add-todos mb-5">
    <div>
      <div style={{ backgroundColor: "white" }}>
        <div className="header">
          <div
            className="align-items-center justify-content-between text-center container"
            style={{ marginTop: "5px" }}
          >
            <div className="logo my-0 font-weight-normal h3 ">
              <img
                style={{ float: "left", height: "40px", paddingTop: "6px" }}
                src={img}
              ></img>
              <span
                className="text-center"
                style={{
                  fontWeight: "bolder",
                  marginRight: "12%",
                  paddingTop: "10px",
                  display: "inline-block",
                }}
              >
               Order Fields
              </span>
              <a
                className="text-center btn"
                target="_blank"
                href={`/docs`}
                style={{
                  float: "right",
                  fontWeight: "bold",
                  marginTop: 10,
                  fontSize: "16px",
                  display: "inline-block",
                  background: "#FFE941",
                  color: "#022866",
                }}
              >
                Support
              </a>
            </div>
            {/* <hr style={{marginTop:"1em",marginBottom:"0.5em"}}/> */}
          </div>
        </div>

        <Router history={history}>
          <div className="topnav">
            <a style={{ paddingTop:"10px"}} href={"/?shop=" + shop + "&host=" + host}><FontAwesomeIcon icon={faTag}/><strong> Order List</strong></a>
            <a style={{ paddingTop:"10px"}} href={"/setup?shop=" + shop + "&host=" + host}>
           <FontAwesomeIcon icon={faCogs}/><strong>Setup</strong>
           
             
            </a>
            <a style={{ float:'right'}}href={"/plan?shop=" + shop + "&host=" + host}>
            <button type="button" class="btn btn-primary"> <FontAwesomeIcon icon={faList}/> Plans </button></a>
          </div>
        </Router>

        {/* <h5
          style={{ width: "100%", marginTop: "10px" }}
          className="text-center"
        >
          {" "}
          <strong>Plan</strong>
        </h5> */}
          












           <div className="container mt-5">
    {/* <div className="pricing card-deck flex-column flex-md-row mb-3"> */}
    <div style={{ display: 'flex',
  marginLeft: 'auto',
  marginRight: 'auto',
  width: "50%"}}>
        <div className={"card card-pricing " +
        // (plan_id == 1 ? "popular shadow " : "") +
        "text-center px-3 mb-4"}>
           
            <span className="h6 w-60 mx-auto px-4 py-1 rounded-bottom bg-primary text-white shadow-sm">Basic</span>
            <div className="bg-transparent card-header pt-4 border-0">
                <h1 className="h1 font-weight-normal text-primary text-center mb-0" data-pricing-value="15">Free<span className="h6 text-muted ml-2"></span></h1>
            </div>
            <div className="card-body pt-0" style={{padding: "0.25rem"}}>
                <ul className="list-unstyled mb-4" style={{paddingLeft:"0.5"}}>
                    <li> All Shopify Object Metafields Management</li>
                    <li>All Shopify Metafield Types Support</li>
                    <li>Auto Generate Shopify Liquid Script</li>
                    <li>  </li>
                    <li>  </li>
                    <li> </li>
                     <li>  </li>
                    <li>  </li>
                    <li> </li>
                    <li>  </li>
                    <li>  </li>
                    <li> </li>
                    <li>  </li>
                    <li> </li>
                    <li>  </li>
                    <li> </li>
                    <li>  </li>
                    <li> </li>
                    <li>  </li>
                    <li> </li>
                    <li>  </li>
                   
                </ul>
                <button type="button" style={{backgroundColor:"rgb(255, 233, 65)",color: "rgb(2, 40, 102)"}} className="text-center btn" disabled>Active</button>
                                {/* {plan_id == 1 ?(
                <button type="button" style={{backgroundColor:"rgb(255, 233, 65)",color: "rgb(2, 40, 102)"}} className="text-center btn" disabled>Active</button>
                ) :<a target="_parent" href= {"https://"+(shop)+"/admin/apps/billing/1"} style={{backgroundColor:"rgb(2,40,102)",color: "#ffffff"}}  className="btn btn-outline-secondary mb-3">Order now</a> }  */}
            </div>
        </div>
        <div style={{marginLeft:'15px'}} className={"card card-pricing " +
        // (plan_id == 1 ? "popular shadow " : "") +
        "text-center px-3 mb-4"}>
           
            <span className="h6 w-60 mx-auto px-4 py-1 rounded-bottom bg-primary text-white shadow-sm">Advance</span>
            <div className="bg-transparent card-header pt-4 border-0">
                <h1 className="h1 font-weight-normal text-primary text-center mb-0" data-pricing-value="15">9.99<span className="h6 text-muted ml-2"></span></h1>
            </div>
            <div className="card-body pt-0" style={{padding: "0.25rem"}}>
                <ul className="list-unstyled mb-4" style={{paddingLeft:"0.5"}}>
                <li> All Shopify Object Metafields Management</li>
                    <li>All Shopify Metafield Types Support</li>
                   
                    <li> CSV Import/Export </li>
                    <li style={{color:"rgb(2,40,102)",fontWeight:"bold"}}> 10 Days Free Trial </li>
                     
                   
                   
                </ul>
                <a target="_parent" href= {"https://"+(shop)+"/admin/apps/billing/1"} style={{backgroundColor:"rgb(2,40,102)",color: "#ffffff"}}  className="btn btn-outline-secondary mb-3">Order now</a>                                 {/* {plan_id == 1 ?(
                <button type="button" style={{backgroundColor:"rgb(255, 233, 65)",color: "rgb(2, 40, 102)"}} className="text-center btn" disabled>Active</button>
                ) :<a target="_parent" href= {"https://"+(shop)+"/admin/apps/billing/1"} style={{backgroundColor:"rgb(2,40,102)",color: "#ffffff"}}  className="btn btn-outline-secondary mb-3">Order now</a> }  */}
            </div>
        </div>
        
        
       
    </div>
 
</div>




    <h5 className="text-center" style={{display:"flex",justifyContent:"center",color:"#022866",alignItems:"center",height:"100px"}}> If you have question contact &nbsp;
         
         <a className="text-center " target="_top" href={" mailto:support@arcafy.com"}> support@arcafy.com </a> </h5>



</div>

          </div>
          // </div>
)
}
export default Plan;