import React, { useEffect, useState } from "react";
import Link from "next/link";

 import Routes from "./Routes";
 import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
 import { faFile, faList, faBlog,faTag,faFileImport,faTrash,faAmericanSignLanguageInterpreting,faParagraph,faCogs, faReceipt, faUser, faShoppingBag,faListAlt } from '@fortawesome/free-solid-svg-icons'
 
import img from "./img/arcafy.png";
import { getSessionToken } from "@shopify/app-bridge-utils";
import { useAppBridge } from "@shopify/app-bridge-react";
import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'

import jwt from "jwt-decode"; // import dependency

const Index = (props) => {
  const app = useAppBridge();

  const [shop, setShop] = useState();
  const [host, setHost] = useState();

  useEffect(() => {
    sessionDecode();
  });

  const sessionDecode = async () => {
    const token = await getSessionToken(app);
    const user_token = jwt(token);

    const hostdecode = user_token.iss.replace(/https:\/\//, "");
    setShop(hostdecode.replace("/admin", ""));

    const encodedString = Buffer.from(hostdecode).toString("base64");
    setHost(encodedString.replace("==", ""));
  };

  return (
    // <div className="container py-15">
    //   <div className="add-todos mb-5">
    <div>
        <div className="header">
          <div
            className="align-items-center justify-content-between text-center container"
            style={{ marginTop: "5px" }}
          >
            <div className="logo my-0 font-weight-normal h3 ">
              <img
                style={{ float: "left", height: "40px", paddingTop: "6px" }}
                src={img}
              ></img>

 
              <span
                className="text-center"
                style={{
                  fontWeight: "bolder",
                  marginRight: "12%",
                  paddingTop: "10px",
                  display: "inline-block",
                }}
              >
              Order Fields
              </span>
              <a
                className="text-center btn"
                target="_blank"
                href={`/docs`}
                style={{
                  float: "right",
                  fontWeight: "bold",
                  marginTop: 10,
                  fontSize: "16px",
                  display: "inline-block",
                  background: "#FFE941",
                  color: "#022866",
                }}
              >
                Support
              </a>
            </div>
            {/* <hr style={{marginTop:"1em",marginBottom:"0.5em"}}/> */}
          </div>
        </div>







        <div className="topnav">
          <a href={"/?shop=" + shop + "&host=" + host}>
           
              <b><button type="button" class="btn btn-primary" style={{height:"36px",paddingTop:"4px",width:"116%"}}><FontAwesomeIcon icon={faTag}/> Order List</button></b>
           
          </a>
          <a style={{paddingTop:"10px"}} href={"/setup?shop=" + shop + "&host=" + host}>
          <strong><FontAwesomeIcon icon={faCogs}/> Setup </strong>
          </a>

          {/* <a style={{paddingTop:"10px"}} href={"/setup?shop=" + shop + "&host=" + host}>
          Setup
          </a> */}
          <a  style={{
                 float:'right',
                 paddingTop:"10px"
                }}  href={"/plan?shop=" + shop + "&host=" + host}><strong><FontAwesomeIcon icon={faList}/> Plans</strong></a>
        </div>





        <div>
          <Routes />
        </div>
      </div>
    
  );
};

Index.getInitialProps = async ({ props }) => {
  let title = "Latest post on next js";
  let slug = "latest post next js";
  return {
    title,
    slug,
  };
};
export default Index;

    // "@shopify/koa-shopify-auth": "^4.1.2",
    // "scripts": {
    //   "test": "echo \"Error: no test specified\" && exit 1",
    //   "dev": "node server.js",
    //   "build": "next build",
    //   "start": "NODE_ENV=production node server.js"
    // },