require('isomorphic-fetch');
const dotenv = require('dotenv');
const Koa = require('koa');
const next = require('next');
const { default: createShopifyAuth } = require('@shopify/koa-shopify-auth');
const { verifyRequest } = require('@shopify/koa-shopify-auth');
const { default: Shopify, ApiVersion,DataType } = require('@shopify/shopify-api');

// import Shopify from '@shopify/shopify-api'
// import {ApiVersion,DataType} from '@shopify/shopify-api'

const Router = require('koa-router');
const getSubscriptionUrl = require('./server/getSubscriptionUrl');
const koaBody = require("koa-body");

const {
  storeCallback,
  loadCallback,
  deleteCallback,
  getSessionId,
} = require("./server/database");
const {
  getFieldList,
  getFieldListIsActive,
  saveFieldData,
  saveFieldAdminData,
  deleteFieldAdminData,
  updateActiveField,
  getFieldsToListing,
  isFieldAvail,
} = require("./server/OrderAPI");
dotenv.config();

Shopify.Context.initialize({
  API_KEY: process.env.SHOPIFY_API_KEY,
  API_SECRET_KEY: process.env.SHOPIFY_API_SECRET,
  SCOPES: process.env.SHOPIFY_API_SCOPES.split(","),
  HOST_NAME: process.env.SHOPIFY_APP_URL.replace(/https:\/\//, ""),
  API_VERSION: ApiVersion.January21,
  IS_EMBEDDED_APP: true,
  // SESSION_STORAGE: new Shopify.Session.MemorySessionStorage(),
  SESSION_STORAGE: new Shopify.Session.CustomSessionStorage(
    storeCallback,
    loadCallback,
    deleteCallback
  ),
});

const port = parseInt(process.env.PORT, 10) || 5000;
const dev = process.env.NODE_ENV !== 'production';
const app = next({ dev: dev });
const handle = app.getRequestHandler();

const ACTIVE_SHOPIFY_SHOPS = {};

app.prepare().then(() => {
  const server = new Koa();
  const router = new Router();
  server.keys = [Shopify.Context.API_SECRET_KEY];

  server.use(
    createShopifyAuth({
      async afterAuth(ctx) {
        const { shop, scope, accessToken } = ctx.state.shopify;
        ACTIVE_SHOPIFY_SHOPS[shop] = scope;

        const registration = await Shopify.Webhooks.Registry.register({
          shop,
          accessToken,
          path: '/webhooks',
          topic: 'APP_UNINSTALLED',
          apiVersion: ApiVersion.January21,
          webhookHandler: (_topic, shop, _body) => {
            console.log('App uninstalled');
            delete ACTIVE_SHOPIFY_SHOPS[shop];
          },
        });

        if (registration.success) {
          console.log('Successfully registered webhook!');
        } else {
          console.log('Failed to register webhook', registration.result);
        }
        const returnUrl = `https://${Shopify.Context.HOST_NAME}/?shop=${shop}`;
        const subscriptionUrl = await getSubscriptionUrl(accessToken, shop, returnUrl);
        ctx.redirect(subscriptionUrl);
      },
    }),
  );

  router.post("/graphql", verifyRequest({ returnHeader: true }), async (ctx, next) => {
    await Shopify.Utils.graphqlProxy(ctx.req, ctx.res);
  });

  router.post('/webhooks', async (ctx) => {
    await Shopify.Webhooks.Registry.process(ctx.req, ctx.res);
    console.log(`Webhook processed with status code 200`);
  });

  const handleRequest = async (ctx) => {
    await handle(ctx.req, ctx.res);
    ctx.respond = false;
    ctx.res.statusCode = 200;
  };

  

  router.get("(/_next/static/.*)", handleRequest);
  router.get("/_next/webpack-hmr", handleRequest);
  // router.get("(.*)", verifyRequest(), handleRequest);




  //**************************************************************************** */

  router.get("/getOrder", async (ctx) => {
    const session = await Shopify.Utils.loadCurrentSession(ctx.req, ctx.res);
    const shop = session.shop;

    //console.log(shop)

    const client = new Shopify.Clients.Rest(shop, session.accessToken);
    const data = await client.get({
      path: "orders",
      query: { status: "any" },
    });

    // our object array
    let data_array = [];

    const array1 = data.body.orders;
    // load data into object

    //console.log(array1);
    for (var i = 0; i < array1.length; i++) {
      // our object
      let my_object = {};

    //  console.log("customer data--", array1[i].customer);
      my_object.id = array1[i].id;
      my_object.order_number = array1[i].order_number;
      if (array1[i].customer == undefined) {
        my_object.first_name = "";
        my_object.last_name = "";
      } else {
        my_object.first_name = array1[i].customer.first_name;
        my_object.last_name = array1[i].customer.last_name;
      }
    
      my_object.created_at = array1[i].created_at;
      my_object.email = array1[i].email;

      for (var j = 0; j < array1[i].note_attributes.length; j++) {
        var fieldname = array1[i].note_attributes[j].name;

        my_object[fieldname] = array1[i].note_attributes[j].value;
      }
      data_array.push(my_object);
    }

    //console.log(data_array);
    ctx.body = {
      status: "OK_SETTINGS",
      data: data_array,
    };
  });

  router.get("/getFields", async (ctx) => {
    const session = await Shopify.Utils.loadCurrentSession(ctx.req, ctx.res);
    const shop = session.shop;

    let data = await getFieldList(shop);
    ctx.body = {
      status: "OK_SETTINGS",
      data: data,
    };
  });

  router.get("/getFieldsIsActive", async (ctx) => {
    const session = await Shopify.Utils.loadCurrentSession(ctx.req, ctx.res);
    const shop = session.shop;
    let data = await getFieldListIsActive(shop);

    ctx.body = {
      status: "OK_SETTINGS",
      data: data,
    };
  });

  router.post("/saveFields", koaBody(), async (ctx) => {
    let data = JSON.stringify(ctx.request.body);
    var obj = JSON.parse(data);
    var keys = Object.keys(obj);
    for (var i = 0; i < keys.length; i++) {
      const result = await saveFieldData(keys[i], obj[keys[i]], obj.shop);
    }

    ctx.body = {
      status: "Data inserted successfully!",
    };
  });

  router.post("/savefieldsadmindata", koaBody(), async (ctx) => {
    const session = await Shopify.Utils.loadCurrentSession(ctx.req, ctx.res);
    const shop = session.shop;

    let data = JSON.stringify(ctx.request.body);
    var obj = JSON.parse(data);
    const result1 = await isFieldAvail(
      obj.fieldsname,
      shop,
     
    );
   // console.log('result1',result1.length)



if(result1.length == 0)
{
     const result2 = await saveFieldAdminData(
       obj.fieldsname,
       shop,
       obj.active,
       obj.field_type
     );
   // console.log(result2)
    ctx.body = {
      status: "200",
      success: "Data Inserted successfully!",
    };
  }else
  {
    ctx.body = {
      status: "201",
      success: "Field already exist!",
    };
  }
  });

  router.post("/deletefield", koaBody(), async (ctx) => {
    const session = await Shopify.Utils.loadCurrentSession(ctx.req, ctx.res);
    const shop = session.shop;

    let data = JSON.stringify(ctx.request.body);
    var obj = JSON.parse(data);
    const result = await deleteFieldAdminData(obj.id, shop);
    ctx.body = {
      status: "200",
      success: "Record Deleted successfully!",
    };
  });

  router.post("/updateActiveField", koaBody(), async (ctx) => {
    const session = await Shopify.Utils.loadCurrentSession(ctx.req, ctx.res);
    const shop = session.shop;

    let data = JSON.stringify(ctx.request.body);
    var obj = JSON.parse(data);

    const result = await updateActiveField(obj.active, obj.id, shop);
    ctx.body = {
      status: "200",
      success: "Record Updated successfully!",
    };
  });

  router.get("/shopifyScriptCartPageCode", async (ctx) => {
    const session = await Shopify.Utils.loadCurrentSession(ctx.req, ctx.res);
    const shop = session.shop;

    const client = new Shopify.Clients.Rest(shop, session.accessToken);
    const datatheme = await client.get({
      path: "themes",
    });
   // console.log('datatheme-',datatheme)

    let datajson = JSON.stringify(datatheme.body.themes);
    var arr = JSON.parse(datajson);

    var theme_id = "";

    for (var i = 0; i < arr.length; i++) {
      if (arr[i].role == "main") {
        theme_id = arr[i].id;
      }
    }
    try {
      const cartpagedata = await client.get({
        path: "themes/" + theme_id + "/assets",
        query: { "asset[key]": "sections/cart-template.liquid" },
      });
      ctx.body = {
        status: "OK_SETTINGS",
        data: cartpagedata.body,
      };
    }
    catch (e) {
     // console.log('500',e)

      ctx.body = {
        status: "500",
        data: 'sections/cart-template.liquid file not Found, Please copy below snippet inside <form> tag of cart page',
      };
      return;
    }
  

   
  });
  router.post("/scriploaded", koaBody(), async (ctx) => {
    const session = await Shopify.Utils.loadCurrentSession(ctx.req, ctx.res);
    const shop = session.shop;

    let data = JSON.stringify(ctx.request.body);
    var obj = JSON.parse(data);

    const client = new Shopify.Clients.Rest(shop, session.accessToken);

    const datascriptload = await client.put({
      path: "themes/" + obj.theme_id + "/assets",
      data: {
        asset: {
          key: "sections/cart-template.liquid",
          value: "" + obj.script + "",
        },
      },
      type: DataType.JSON,
    });
    //console.log('loaded',datascriptload.body)
    ctx.body = {
      status: "OK_SETTINGS",
      data: datascriptload.body,
    };
  });
  router.get("/getFieldsToListing", async (ctx) => {
    const session = await Shopify.Utils.loadCurrentSession(ctx.req, ctx.res);
    const shop = session.shop;
    const result = await getFieldsToListing(shop);

    ctx.body = {
      status: "OK_SETTINGS",
      data: result,
    };
  });
  router.post("/getOrderByFieldName", koaBody(), async (ctx) => {
    const session = await Shopify.Utils.loadCurrentSession(ctx.req, ctx.res);
    const shop = session.shop;
    let dataJSON = JSON.stringify(ctx.request.body);
    var obj = JSON.parse(dataJSON);
    //console.log(obj.order_id);
    const client = new Shopify.Clients.Rest(shop, session.accessToken);
    const data = await client.get({
      path: "orders/" + obj.order_id,
    });

    ctx.body = {
      status: "OK_SETTINGS",
      data: data.body,
    };
  });








  router.get("(.*)", async (ctx) => {
    const shop = ctx.query.shop;

    if (ACTIVE_SHOPIFY_SHOPS[shop] === undefined) {
      ctx.redirect(`/auth?shop=${shop}`);
    } else {
      await handleRequest(ctx);
    }
  });
  server.use(router.allowedMethods());
  server.use(router.routes());

  server.listen(port, () => {
    console.log(`> server runing on port :${port}`);
  });
});
