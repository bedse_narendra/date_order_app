require("dotenv").config();
const webpack = require('webpack');
const withImages = require("next-images");

const apiKey = JSON.stringify(process.env.SHOPIFY_API_KEY);

module.exports = withImages({
  fileExtensions: ["jpg", "jpeg", "png", "gif"],
  webpack: (config) => {
    const env = {API_KEY: apiKey};
    config.plugins.push(new webpack.DefinePlugin(env));
    return config;
  },
});
